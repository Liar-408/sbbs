# 项目实践—沙软论坛（SBBS）

## 功能模块：

* 用户管理
  * 注册（要做绑定邮箱功能吗）
  * 登录
  * 用户通知
  * 修改账号信息
  * 注销账号
  * 用户等级
* 帖子管理
  * 发布帖子（暂且不含图片）
  * 删除帖子
  * 评论帖子
  * 回复楼层
  * 点赞帖子
  * 收藏帖子
  * 搜索功能
* 

## 数据库设计：

* 顶层数据库及对应用户

  * 用户信息表：
    * 账户名兼昵称name	VARCHAR(30) unique
    * 密码pwd    VARCHAR(20) 
    * 发布的帖子 用id关联
    * [拓展] 头像icon    VARCHAR(100)

  * 帖子表：
    * 帖子名name VARCHAR(30)
    * 发布用户的名字poster VARCHAR(30) 
    * 帖子楼层数floor 
  * 帖子楼层表：
    * 所属帖子
    * 该楼层的楼层数
    * 回复的楼层数
    * 楼层内容
  * 回复表：
    * 回复的对象（是楼层本身，还是回复楼中楼）
    * 回复的内容
    * 回复内容

## 开发人员

* **ISEKAI：后端**
* **MURAMASA：前端**
* **若无：AI模型构建**

